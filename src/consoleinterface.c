/*
    LANWakeup - Wake On LAN program for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of LANWakeup.

    LANWakeup is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//LANWakeup (2010 - Steven Rodriguez -)                                //
//=====================================================================//
#if defined(LANWAKEUP_UI_CONSOLE)
//===========================================================//
//Libraries                                                  //
//===========================================================//

#include <wakeonlan.h>
#include <libintl.h>
#include "consoleinterface.h"

//===========================================================//
//Functions                                                  //
//===========================================================//

void LANWakeup_Console_LoadInterface(int argc, char *argv[], struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 udpPort, struct SecureOnPassword secureOnPassword, WakeOnLAN_UInt8 useSecureOnPassword)
{
	//Option variables
	struct MACAddress mac = macAddress;
	struct IPAddress ip = ipAddress;
	WakeOnLAN_UInt16 port = udpPort;
	struct SecureOnPassword secureonpassword = secureOnPassword;
	WakeOnLAN_UInt8 usesecureonpassword;

	//Main option loop related variables
	WakeOnLAN_UInt8 mainloop = WAKEONLAN_TRUE;
	char *buffer;

	printf(gettext("Welcome to Digitecnology LANWakeup!!!\n"));
	printf("=====================================\n\n");
	printf(gettext("Using WakeOnLANLIB v%s.\n"), WakeOnLAN_GetString(WAKEONLAN_VERSION));
	printf(gettext("WakeOnLANLIB vendor: %s.\n"), WakeOnLAN_GetString(WAKEONLAN_VENDOR));
	printf(gettext("WakeOnLANLIB compile date: %s.\n\n"), WakeOnLAN_GetString(WAKEONLAN_COMPILE_DATE));


	//Option main loop
	while(mainloop == WAKEONLAN_TRUE)
	{
		//Ask for MAC address
		mac = LANWakeup_Console_AskForMACAddress(macAddress);

		buffer = WakeOnLAN_CreateStringFromMACAddress(mac);
		printf(gettext("MAC Address selected: %s.\n\n"), buffer);
		g_free(buffer);

		//Ask for use SecureOn (tm) password
		if(LANWakeup_Console_AskForOption(gettext("Do you want to use a SecureOn (tm) password?")) == WAKEONLAN_TRUE)
		{
			//Ask for SecureOn (tm) password
			secureonpassword = LANWakeup_Console_AskForSecureOnPassword(secureOnPassword);
			usesecureonpassword = WAKEONLAN_TRUE;

			buffer = WakeOnLAN_CreateStringFromSecureOnPassword(secureonpassword);
			printf(gettext("SecureOn (tm) password selected: %s.\n\n"), buffer);
			g_free(buffer);
		}
		else
		{
			secureonpassword = secureOnPassword;
			usesecureonpassword = WAKEONLAN_FALSE;
		}

		//Ask for IP address
		ip = LANWakeup_Console_AskForIPAddress(ipAddress);

		buffer = WakeOnLAN_CreateStringFromIPAddress(ip);
		printf(gettext("IP Address selected: %s.\n\n"), buffer);
		g_free(buffer);

		//Ask for UDP port
		port = LANWakeup_Console_AskForUDPPort(udpPort);
		printf(gettext("UDP port selected: %hu.\n\n"), port);

		//Wakeup machine
		printf(gettext("Waking up machine...\n"));

		if(WakeOnLAN_WakeupMachine(mac, ip, port, secureonpassword, usesecureonpassword) == WAKEONLAN_TRUE)
		{
			printf(gettext("Done.\n\n"));
		}
		else
		{
			printf(gettext("Failed to wakeup, %s.\n\n"), WakeOnLAN_GetErrorString());
			WakeOnLAN_ClearErrors();
		}

		//Ask for wake up another machine
		if(LANWakeup_Console_AskForOption(gettext("Do you want to wakeup another machine?")) == WAKEONLAN_FALSE)
		{
			mainloop = WAKEONLAN_FALSE;
		}
	}

	//Put the exit message
	printf(gettext("Bye.\n"));
}

struct MACAddress LANWakeup_Console_AskForMACAddress(struct MACAddress currentMACAddress)
{
	//Loop related variables
	WakeOnLAN_UInt8 optionloop = WAKEONLAN_TRUE;
	char *buffer;

	//Option related variables
	struct MACAddress selectedmac;

	//Run the option loop
	while(optionloop == WAKEONLAN_TRUE)
	{
		buffer = WakeOnLAN_CreateStringFromMACAddress(currentMACAddress);
		printf(gettext("Put the machine MAC address to wakeup [default: %s]: "), buffer);
		g_free(buffer);

		buffer = LANWakeup_Console_AskString();

		if(g_strcmp0(buffer, "") == 0)
		{
			selectedmac = currentMACAddress;
			optionloop = WAKEONLAN_FALSE;
		}
		else
		{
			selectedmac = WakeOnLAN_CreateMACAddressFromString(buffer);

			if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
			{
				selectedmac = currentMACAddress;
				printf(gettext("Please, write a correct MAC address.\n\n"));
				WakeOnLAN_ClearErrors();
			}
			else
			{
				optionloop = WAKEONLAN_FALSE;
			}
		}

		g_free(buffer);
	}

	return selectedmac;
}

struct SecureOnPassword LANWakeup_Console_AskForSecureOnPassword(struct SecureOnPassword currentSecureOnPassword)
{
	//Loop related variables
	WakeOnLAN_UInt8 optionloop = WAKEONLAN_TRUE;
	char *buffer;

	//Option related variables
	struct SecureOnPassword selectedsecureonpassword;

	//Run the option loop
	while(optionloop == WAKEONLAN_TRUE)
	{
		buffer = WakeOnLAN_CreateStringFromSecureOnPassword(currentSecureOnPassword);
		printf(gettext("Put the machine SecureOn (tm) password to wakeup [default: %s]: "), buffer);
		g_free(buffer);

		buffer = LANWakeup_Console_AskString();

		if(g_strcmp0(buffer, "") == 0)
		{
			selectedsecureonpassword = currentSecureOnPassword;
			optionloop = WAKEONLAN_FALSE;
		}
		else
		{
			selectedsecureonpassword = WakeOnLAN_CreateSecureOnPasswordFromString(buffer);

			if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
			{
				selectedsecureonpassword = currentSecureOnPassword;
				printf(gettext("Please, write a correct SecureOn (tm) password.\n\n"));
				WakeOnLAN_ClearErrors();
			}
			else
			{
				optionloop = WAKEONLAN_FALSE;
			}
		}

		g_free(buffer);
	}

	return selectedsecureonpassword;
}

struct IPAddress LANWakeup_Console_AskForIPAddress(struct IPAddress currentIPAddress)
{
	//Loop related variables
	WakeOnLAN_UInt8 optionloop = WAKEONLAN_TRUE;
	char *buffer;

	//Option related variables
	struct IPAddress selectedip;

	//Run the option loop
	while(optionloop == WAKEONLAN_TRUE)
	{
		buffer = WakeOnLAN_CreateStringFromIPAddress(currentIPAddress);
		printf(gettext("Put the machine IP address to wakeup [default: %s]: "), buffer);
		g_free(buffer);

		buffer = LANWakeup_Console_AskString();

		if(g_strcmp0(buffer, "") == 0)
		{
			selectedip = currentIPAddress;
			optionloop = WAKEONLAN_FALSE;
		}
		else
		{
			selectedip = WakeOnLAN_CreateIPAddressFromString(buffer);

			if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
			{
				selectedip = currentIPAddress;
				printf(gettext("Please, write a correct IP address.\n\n"));
				WakeOnLAN_ClearErrors();
			}
			else
			{
				optionloop = WAKEONLAN_FALSE;
			}
		}

		g_free(buffer);
	}

	return selectedip;
}

WakeOnLAN_UInt16 LANWakeup_Console_AskForUDPPort(WakeOnLAN_UInt16 currentUDPPort)
{
	//Loop related variables
	WakeOnLAN_UInt8 optionloop = WAKEONLAN_TRUE;
	char *buffer;

	//Option related variables
	WakeOnLAN_UInt16 selectedudpport;

	//Run the option loop
	while(optionloop == WAKEONLAN_TRUE)
	{
		printf(gettext("Put the machine UDP port to wakeup [default: %hu]: "), currentUDPPort);

		buffer = LANWakeup_Console_AskString();

		if(g_strcmp0(buffer, "") == 0)
		{
			selectedudpport = currentUDPPort;
			optionloop = WAKEONLAN_FALSE;
		}
		else
		{
			if(LANWakeup_Console_GetIntegerFromString(buffer, &selectedudpport) == WAKEONLAN_FALSE)
			{
				selectedudpport = currentUDPPort;
				printf(gettext("Please, write a correct UDP port.\n\n"));
			}
			else
			{
				optionloop = WAKEONLAN_FALSE;
			}
		}

		g_free(buffer);
	}

	return selectedudpport;
}

WakeOnLAN_UInt8 LANWakeup_Console_AskForOption(char *option)
{
	//Loop related variables
	WakeOnLAN_UInt8 optionloop = WAKEONLAN_TRUE;
	char *buffer;

	//Option related variables
	WakeOnLAN_UInt8 selectedoption;

	//Check if option is NULL
	if(option == NULL)
	{
		return WAKEONLAN_FALSE;
	}

	//Run the option loop
	while(optionloop == WAKEONLAN_TRUE)
	{
		printf("%s [y/n]: ", option);

		buffer = LANWakeup_Console_AskString();

		if((g_strcmp0(buffer, "y") == 0) || (g_strcmp0(buffer, "Y") == 0))
		{
			optionloop = WAKEONLAN_FALSE;
			selectedoption = WAKEONLAN_TRUE;
			printf("\n");
		}
		else if((g_strcmp0(buffer, "n") == 0) || (g_strcmp0(buffer, "N") == 0))
		{
			optionloop = WAKEONLAN_FALSE;
			selectedoption = WAKEONLAN_FALSE;
			printf("\n");
		}
		else
		{
			printf(gettext("Please, put a correct option.\n\n"));
		}

		g_free(buffer);
	}

	return selectedoption;
}

char *LANWakeup_Console_AskString()
{
	GString *string = g_string_new(NULL);
	char character;

	while ((character = getchar()) != '\n')
            string = g_string_append_c(string, character);

	return g_string_free(string, FALSE);
}

WakeOnLAN_UInt8 LANWakeup_Console_GetIntegerFromString(char *string, WakeOnLAN_UInt16 *integer)
{
	int i;
	gint64 result;

	//Check if the string is NULL
	if(string == NULL)
	{
		return WAKEONLAN_FALSE;
	}

	//Check if the string length is the max length of uint16
	if(strlen(string) > 5)
	{
		return WAKEONLAN_FALSE;
	}

	//Check if the string has only digits
	for(i = 0; i < strlen(string); i++)
	{
		if(g_ascii_isdigit(string[i]) == FALSE)
		{
			return WAKEONLAN_FALSE;
		}
	}

	//Convert the string to an integer and verify if it is in the uint16 valid ranges
	result = g_ascii_strtoll(string, NULL, 10);

	if(result < 0 || result > G_MAXUINT16)
	{
		return WAKEONLAN_FALSE;
	}

	//Convert the result to an uint16
	*integer = (WakeOnLAN_UInt16)result;

	return WAKEONLAN_TRUE;
}
#endif
