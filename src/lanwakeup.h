/*
    LANWakeup - Wake On LAN program for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of LANWakeup.

    LANWakeup is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//LANWakeup (2010 - Steven Rodriguez -)                                //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file lanwakeup.h
/// \brief LANWakeup header file.
/// \details This is the LANWakeup header file.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include <wakeonlan.h>
#include <libintl.h>
#include <locale.h>

//===========================================================//
//Structures                                                 //
//===========================================================//

////////////////////////////////////////////////////
/// \struct LANWakeupInterface
/// \brief Base LANWakrup interface structure.
////////////////////////////////////////////////////
struct LANWakeupInterface
{
	void (*Load)(int argc, char *argv[], struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 udpPort, struct SecureOnPassword secureOnPassword, WakeOnLAN_UInt8 useSecureOnPassword);
};
