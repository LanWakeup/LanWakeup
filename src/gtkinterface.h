/*
    LANWakeup - Wake On LAN program for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of LANWakeup.

    LANWakeup is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//LANWakeup (2010 - Steven Rodriguez -)                                //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file gtkinterface.h
/// \brief LANWakeup GTK+ interface header file.
/// \details This is the LANWakeup GTK+ interface header file.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include <gtk/gtk.h>

//===========================================================//
//Macros                                                     //
//===========================================================//

////////////////////////////////////////////////////
/// \def GTKBUILDER_GET_WIDGET
/// \brief Utility macro for obtaining object using GTKBuilder.
////////////////////////////////////////////////////
#define GTKBUILDER_GET_WIDGET(x,y) y = (x *)gtk_builder_get_object(builder_lanwakeup_gui,#y)

//===========================================================//
//Functions                                                  //
//===========================================================//

//******************************************************//
//General interface functions                           //
//******************************************************//

////////////////////////////////////////////////////
/// \fn void LANWakeup_GTK_LoadInterface(int argc, char *argv[], struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 udpPort)
/// \brief Loads the GTK+ interface.
/// \param argc The number of the program arguments.
/// \param argv The program arguments.
/// \param macAddress The default machine MAC address to wakeup.
/// \param ipAddress The default machine IP address to wakeup.
/// \param udpPort The default machine UDP port to wakeup.
/// \param secureOnPassword The SecureOn (tm) password to use.
/// \param useSecureOnPassword WAKEONLAN_TRUE to use the SecureOn (tm) password selected, WAKEONLAN_FALSE if not.
////////////////////////////////////////////////////
void LANWakeup_GTK_LoadInterface(int argc, char *argv[], struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 udpPort, struct SecureOnPassword secureOnPassword, WakeOnLAN_UInt8 useSecureOnPassword);

//******************************************************//
//GTK+ related functions                                //
//******************************************************//

gboolean window_lanwakeup_main_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data);

void button_lanwakeup_about_clicked(GtkButton *button, gpointer user_data);

void button_lanwakeup_close_clicked(GtkButton *button, gpointer user_data);

void button_lanwakeup_turnon_clicked(GtkButton *button, gpointer user_data);

void checkbutton_lanwakeup_useadditionaloptions_toggled(GtkToggleButton *togglebutton, gpointer user_data);

void checkbutton_lanwakeup_usesecureonpassword_toggled(GtkToggleButton *togglebutton, gpointer user_data);
