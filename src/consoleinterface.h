/*
    LANWakeup - Wake On LAN program for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of LANWakeup.

    LANWakeup is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//LANWakeup (2010 - Steven Rodriguez -)                                //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file consoleinterface.h
/// \brief LANWakeup Console interface header file.
/// \details This is the LANWakeup Console interface header file.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

//===========================================================//
//Functions                                                  //
//===========================================================//

//******************************************************//
//General interface functions                           //
//******************************************************//

////////////////////////////////////////////////////
/// \fn void LANWakeup_Console_LoadInterface(struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 udpPort)
/// \brief Loads the Console interface.
/// \param argc The number of the program arguments.
/// \param argv The program arguments.
/// \param macAddress The default machine MAC address to wakeup.
/// \param ipAddress The default machine IP address to wakeup.
/// \param udpPort The default machine UDP port to wakeup.
/// \param secureOnPassword The SecureOn (tm) password to use.
/// \param useSecureOnPassword WAKEONLAN_TRUE to use the SecureOn (tm) password selected, WAKEONLAN_FALSE if not.
////////////////////////////////////////////////////
void LANWakeup_Console_LoadInterface(int argc, char *argv[], struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 udpPort, struct SecureOnPassword secureOnPassword, WakeOnLAN_UInt8 useSecureOnPassword);

//******************************************************//
//Console related functions                             //
//******************************************************//

////////////////////////////////////////////////////
/// \fn struct MACAddress LANWakeup_Console_AskForMACAddress(struct MACAddress currentMACAddress)
/// \brief Asks for a MAC address.
/// \param currentMACAddress The current MAC address.
/// \return The MAC address selected.
////////////////////////////////////////////////////
struct MACAddress LANWakeup_Console_AskForMACAddress(struct MACAddress currentMACAddress);

////////////////////////////////////////////////////
/// \fn struct SecureOnPassword LANWakeup_Console_AskForSecureOnPassword(struct SecureOnPassword currentSecureOnPassword)
/// \brief Asks for a SecureOn (tm) password.
/// \param currentMACAddress The current SecureOn (tm) password.
/// \return The SecureOn (tm) password selected.
////////////////////////////////////////////////////
struct SecureOnPassword LANWakeup_Console_AskForSecureOnPassword(struct SecureOnPassword currentSecureOnPassword);

////////////////////////////////////////////////////
/// \fn struct IPAddress LANWakeup_Console_AskForIPAddress(struct IPAddress currentIPAddress)
/// \brief Asks for a IP address.
/// \param currentIPAddress The current IP address.
/// \return The IP address selected.
////////////////////////////////////////////////////
struct IPAddress LANWakeup_Console_AskForIPAddress(struct IPAddress currentIPAddress);

////////////////////////////////////////////////////
/// \fn WakeOnLAN_UInt16 LANWakeup_Console_AskForUDPPort(WakeOnLAN_UInt16 currentUDPPort)
/// \brief Asks for an UDP port.
/// \param currentUDPPort The current UDP port.
/// \return The UDP port selected.
////////////////////////////////////////////////////
WakeOnLAN_UInt16 LANWakeup_Console_AskForUDPPort(WakeOnLAN_UInt16 currentUDPPort);

////////////////////////////////////////////////////
/// \fn WakeOnLAN_UInt8 LANWakeup_Console_AskForOption()
/// \brief Asks for a specific option.
/// \param option The option to ask.
/// \return LANWAKEUP_TRUE if yes, LANWAKEUP_FALSE if no.
////////////////////////////////////////////////////
WakeOnLAN_UInt8 LANWakeup_Console_AskForOption(char *option);

////////////////////////////////////////////////////
/// \fn char *LANWakeup_Console_AskString()
/// \brief Asks for a string.
/// \return The string asked.
////////////////////////////////////////////////////
char *LANWakeup_Console_AskString();

////////////////////////////////////////////////////
/// \fn WakeOnLAN_UInt8 LANWakeup_Console_GetIntegerFromString(char *string, WakeOnLAN_UInt16 *integer)
/// \brief Gets the unsigned 16-bit integer part from a string.
/// \param string The string containing the unsigned 16-bit integer.
/// \param integer The unsigned 16-bit integer for containing the result.
/// \return WAKEONLAN_TRUE if teh conversion was sucessfull or WAKEONLAN_FALSE if not.
////////////////////////////////////////////////////
WakeOnLAN_UInt8 LANWakeup_Console_GetIntegerFromString(char *string, WakeOnLAN_UInt16 *integer);
