/*
    LANWakeup - Wake On LAN program for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of LANWakeup.

    LANWakeup is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//LANWakeup (2010 - Steven Rodriguez -)                                //
//=====================================================================//
#if defined(LANWAKEUP_UI_GTK)
//===========================================================//
//Libraries                                                  //
//===========================================================//

#include <wakeonlan.h>
#include <libintl.h>
#include "gtkinterface.h"

//===========================================================//
//Global Variables                                           //
//===========================================================//

//GTK+ Builder
GtkBuilder *builder_lanwakeup_gui;

//LANWakeup GUI
GtkWidget *window_lanwakeup_main;
GtkLabel *label_lanwakeup_macaddress;
GtkEntry *entry_lanwakeup_macaddress;
GtkLabel *label_lanwakeup_additionaloptions;
GtkCheckButton *checkbutton_lanwakeup_useadditionaloptions;
GtkLabel *label_lanwakeup_ipaddress;
GtkEntry *entry_lanwakeup_ipaddress;
GtkLabel *label_lanwakeup_udpport;
GtkSpinButton *spinbutton_lanwakeup_udpport;
GtkLabel *label_lanwakeup_secureonpasswordoptions;
GtkCheckButton *checkbutton_lanwakeup_usesecureonpassword;
GtkLabel *label_lanwakeup_secureonpassword;
GtkEntry *entry_lanwakeup_secureonpassword;
GtkButton *button_lanwakeup_about;
GtkButton *button_lanwakeup_close;
GtkButton *button_lanwakeup_turnon;
GtkStatusbar *statusbar_lanwakeup_status;

//Status messages context id
guint contextid;

//About dialog
GtkAboutDialog *aboutdialog_lanwakeup_about;


//===========================================================//
//Functions                                                  //
//===========================================================//

void LANWakeup_GTK_LoadInterface(int argc, char *argv[], struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 udpPort, struct SecureOnPassword secureOnPassword, WakeOnLAN_UInt8 useSecureOnPassword)
{
	GError *error = NULL;

	//Initialize i18n support
  	gtk_set_locale ();

	//Initialize GTK+
	gtk_init (&argc, &argv);

	//Load the GTK+ builder and connect the signals
	builder_lanwakeup_gui = gtk_builder_new();

	if(gtk_builder_add_from_file(builder_lanwakeup_gui, "LANWakeup.ui", &error) == 0)
	{
		printf(gettext("Error building the GTK+ interface.\n"));
		printf(gettext("Error: %s.\n"), error->message);
		g_error_free(error);
		return;
	}

	gtk_builder_connect_signals(builder_lanwakeup_gui, NULL);

	//Get the objects
	GTKBUILDER_GET_WIDGET(GtkWidget, window_lanwakeup_main);
	GTKBUILDER_GET_WIDGET(GtkLabel, label_lanwakeup_macaddress);
	GTKBUILDER_GET_WIDGET(GtkEntry, entry_lanwakeup_macaddress);
	GTKBUILDER_GET_WIDGET(GtkLabel, label_lanwakeup_additionaloptions);
	GTKBUILDER_GET_WIDGET(GtkCheckButton, checkbutton_lanwakeup_useadditionaloptions);
	GTKBUILDER_GET_WIDGET(GtkLabel, label_lanwakeup_ipaddress);
	GTKBUILDER_GET_WIDGET(GtkEntry, entry_lanwakeup_ipaddress);
	GTKBUILDER_GET_WIDGET(GtkLabel, label_lanwakeup_udpport);
	GTKBUILDER_GET_WIDGET(GtkLabel, label_lanwakeup_secureonpasswordoptions);
	GTKBUILDER_GET_WIDGET(GtkCheckButton, checkbutton_lanwakeup_usesecureonpassword);
	GTKBUILDER_GET_WIDGET(GtkLabel, label_lanwakeup_secureonpassword);
	GTKBUILDER_GET_WIDGET(GtkEntry, entry_lanwakeup_secureonpassword);
	GTKBUILDER_GET_WIDGET(GtkSpinButton, spinbutton_lanwakeup_udpport);
	GTKBUILDER_GET_WIDGET(GtkButton, button_lanwakeup_about);
	GTKBUILDER_GET_WIDGET(GtkButton, button_lanwakeup_close);
	GTKBUILDER_GET_WIDGET(GtkButton, button_lanwakeup_turnon);
	GTKBUILDER_GET_WIDGET(GtkStatusbar, statusbar_lanwakeup_status);
	GTKBUILDER_GET_WIDGET(GtkAboutDialog, aboutdialog_lanwakeup_about);

	//Assign the i18n support
	gtk_label_set_text(label_lanwakeup_macaddress, gettext("MAC Address:"));
	gtk_label_set_text(label_lanwakeup_additionaloptions, gettext("Additional options"));
	gtk_button_set_label((GtkButton *)checkbutton_lanwakeup_useadditionaloptions, gettext("Use additional options"));
	gtk_label_set_text(label_lanwakeup_ipaddress, gettext("IP Address:"));
	gtk_label_set_text(label_lanwakeup_udpport, gettext("UDP port:"));
	gtk_label_set_text(label_lanwakeup_secureonpasswordoptions, gettext("SecureOn (tm) password options"));
	gtk_button_set_label((GtkButton *)checkbutton_lanwakeup_usesecureonpassword, gettext("Use SecureOn (tm) password"));
	gtk_label_set_text(label_lanwakeup_secureonpassword, gettext("SecureOn (tm) password:"));
	gtk_button_set_label(button_lanwakeup_about, gettext("About"));
	gtk_button_set_label(button_lanwakeup_close, gettext("Close"));
	gtk_button_set_label(button_lanwakeup_turnon, gettext("Turn on!"));

	//Assign the i18n support in about dialog
	gtk_about_dialog_set_comments(aboutdialog_lanwakeup_about, gettext("A program for waking up machines"));
	gtk_about_dialog_set_website_label(aboutdialog_lanwakeup_about, gettext("Made by Digitecnology"));
	gtk_window_set_title(GTK_WINDOW(aboutdialog_lanwakeup_about), gettext("About Digitecnology LANWakeup"));

	//Prepare the status bar
	contextid = gtk_statusbar_get_context_id(statusbar_lanwakeup_status, "Main messages");
	gtk_statusbar_push(statusbar_lanwakeup_status, contextid, gettext("Welcome to LANWakeup!"));

	//Put MAC address, IP address and UDP port
	gtk_entry_set_text(entry_lanwakeup_macaddress, WakeOnLAN_CreateStringFromMACAddress(macAddress));

	if((g_strcmp0("255.255.255.255", WakeOnLAN_CreateStringFromIPAddress(ipAddress)) != 0) || udpPort != 40000)
	{
		gtk_toggle_button_set_active((GtkToggleButton *)checkbutton_lanwakeup_useadditionaloptions, TRUE);
                gtk_entry_set_text(entry_lanwakeup_ipaddress, WakeOnLAN_CreateStringFromIPAddress(ipAddress));
                gtk_spin_button_set_value(spinbutton_lanwakeup_udpport, (gdouble)udpPort);
                gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_ipaddress, TRUE);
                gtk_widget_set_sensitive((GtkWidget *)spinbutton_lanwakeup_udpport, TRUE);
	}
	else
	{
		gtk_toggle_button_set_active((GtkToggleButton *)checkbutton_lanwakeup_useadditionaloptions, FALSE);
		gtk_entry_set_text(entry_lanwakeup_ipaddress, "255.255.255.255");
		gtk_spin_button_set_value(spinbutton_lanwakeup_udpport, 40000.0);
		gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_ipaddress, FALSE);
		gtk_widget_set_sensitive((GtkWidget *)spinbutton_lanwakeup_udpport, FALSE);
	}

	//Put SecureOn (tm) password
	gtk_entry_set_text(entry_lanwakeup_secureonpassword, WakeOnLAN_CreateStringFromSecureOnPassword(secureOnPassword));

	if(useSecureOnPassword == WAKEONLAN_TRUE)
	{
		gtk_toggle_button_set_active((GtkToggleButton *)checkbutton_lanwakeup_usesecureonpassword, TRUE);
                gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_secureonpassword, TRUE);
	}
	else
	{
		gtk_toggle_button_set_active((GtkToggleButton *)checkbutton_lanwakeup_usesecureonpassword, FALSE);
		gtk_entry_set_text(entry_lanwakeup_secureonpassword, "00:00:00:00:00:00");
                gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_secureonpassword, FALSE);
	}

	//Show the main window
	gtk_widget_show(window_lanwakeup_main);

	//Enter the main GTK+ loop
	gtk_main ();
}

gboolean window_lanwakeup_main_delete_event(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	//Exit LANWakeup GTK+ interface
	gtk_main_quit();
}

void button_lanwakeup_about_clicked(GtkButton *button, gpointer user_data)
{
	//Show about dialog
	gtk_dialog_run(GTK_DIALOG(aboutdialog_lanwakeup_about));
	gtk_widget_hide(GTK_WIDGET(aboutdialog_lanwakeup_about));
}

void button_lanwakeup_close_clicked(GtkButton *button, gpointer user_data)
{
	//Exit LANWakeup GTK+ interface
	gtk_main_quit();
}

void button_lanwakeup_turnon_clicked(GtkButton *button, gpointer user_data)
{
	//WakeOnLAN parameter variables
	struct MACAddress macaddress;
	struct SecureOnPassword secureonpassword;
	struct IPAddress ipaddress;
	WakeOnLAN_UInt16 udpport;

	//GTK+ Message dialog
	GtkWidget *dialog;

	//Check MAC address
	macaddress = WakeOnLAN_CreateMACAddressFromString((char *)gtk_entry_get_text(entry_lanwakeup_macaddress));

	if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
	{
		//Show error dialog
		dialog = gtk_message_dialog_new(GTK_WINDOW(window_lanwakeup_main), GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, gettext("Please, put a valid MAC address."));
                gtk_dialog_run(GTK_DIALOG(dialog));
                gtk_widget_destroy(dialog);
		gtk_statusbar_push(statusbar_lanwakeup_status, contextid, gettext("MAC address was invalid."));
		WakeOnLAN_ClearErrors();
		return;
	}

	//Check SecureOn (tm) password
	secureonpassword = WakeOnLAN_CreateSecureOnPasswordFromString((char *)gtk_entry_get_text(entry_lanwakeup_secureonpassword));

	if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
	{
		//Show error dialog
		dialog = gtk_message_dialog_new(GTK_WINDOW(window_lanwakeup_main), GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, gettext("Please, put a valid SecureOn (tm) password."));
               	gtk_dialog_run(GTK_DIALOG(dialog));
               	gtk_widget_destroy(dialog);
		gtk_statusbar_push(statusbar_lanwakeup_status, contextid, gettext("SecureOn (tm) password was invalid."));
		WakeOnLAN_ClearErrors();
		return;
	}

	//Check IP address
	ipaddress = WakeOnLAN_CreateIPAddressFromString((char *)gtk_entry_get_text(entry_lanwakeup_ipaddress));

        if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
        {
		//Show error dialog
                dialog = gtk_message_dialog_new(GTK_WINDOW(window_lanwakeup_main), GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, gettext("Please, put a valid IP address."));
		gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		gtk_statusbar_push(statusbar_lanwakeup_status, contextid, gettext("IP address was invalid."));
                WakeOnLAN_ClearErrors();
		return;
        }

	//Obtain UDP port
	udpport = (WakeOnLAN_UInt16)gtk_spin_button_get_value_as_int(spinbutton_lanwakeup_udpport);

	//Send wakeup packet
	if(WakeOnLAN_WakeupMachine(macaddress, ipaddress, udpport, secureonpassword, (WakeOnLAN_UInt8)gtk_toggle_button_get_active((GtkToggleButton *)checkbutton_lanwakeup_usesecureonpassword)) == WAKEONLAN_FALSE)
	{
		//Show error dialog
		dialog = gtk_message_dialog_new(GTK_WINDOW(window_lanwakeup_main), GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, gettext("Error sending the wakeup packet."));
                gtk_dialog_run(GTK_DIALOG(dialog));
                gtk_widget_destroy(dialog);
		gtk_statusbar_push(statusbar_lanwakeup_status, contextid, gettext("Can't send the wakeup packet."));
		WakeOnLAN_ClearErrors();
		return;
	}

	//Show sucessful message
	gtk_statusbar_push(statusbar_lanwakeup_status, contextid, gettext("Machine wakeup sucessful!"));
}

void checkbutton_lanwakeup_useadditionaloptions_toggled(GtkToggleButton *togglebutton, gpointer user_data)
{
	//Change the state of the additional options frame
	if(gtk_toggle_button_get_active(togglebutton) == TRUE)
	{
		gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_ipaddress, TRUE);
                gtk_widget_set_sensitive((GtkWidget *)spinbutton_lanwakeup_udpport, TRUE);
	}
	else
	{
		gtk_entry_set_text(entry_lanwakeup_ipaddress, "255.255.255.255");
                gtk_spin_button_set_value(spinbutton_lanwakeup_udpport, 40000.0);
                gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_ipaddress, FALSE);
                gtk_widget_set_sensitive((GtkWidget *)spinbutton_lanwakeup_udpport, FALSE);
	}
}

void checkbutton_lanwakeup_usesecureonpassword_toggled(GtkToggleButton *togglebutton, gpointer user_data)
{
	//Change the state of the SecureON (tm) password frame
	if(gtk_toggle_button_get_active(togglebutton) == TRUE)
	{
		gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_secureonpassword, TRUE);
	}
	else
	{
		gtk_entry_set_text(entry_lanwakeup_secureonpassword, "00:00:00:00:00:00");
		gtk_widget_set_sensitive((GtkWidget *)entry_lanwakeup_secureonpassword, FALSE);
	}
}
#endif
