/*
    LANWakeup - Wake On LAN program for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of LANWakeup.

    LANWakeup is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//LANWakeup (2010 - Steven Rodriguez -)                                //
//=====================================================================//

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "lanwakeup.h"

#if defined(LANWAKEUP_UI_CONSOLE)
#include "consoleinterface.h"
#endif

#if defined(LANWAKEUP_UI_GTK)
#include "gtkinterface.h"
#endif

//===========================================================//
//Global Variables                                           //
//===========================================================//

//Interface
struct LANWakeupInterface interface;

//Program options
gchar *ipaddress;
gchar *macaddress;
gint udpport = 40000;
gchar *secureonpassword;
#if defined(LANWAKEUP_UI_CONSOLE)
gboolean console = FALSE;
#endif
#if defined(LANWAKEUP_UI_GTK)
gboolean gtk = FALSE;
#endif

//WakeOnLANLIB parameters
struct MACAddress wol_macaddress;
struct IPAddress wol_ipaddress;
WakeOnLAN_UInt16 wol_udpport;
struct SecureOnPassword wol_secureonpassword;
WakeOnLAN_UInt8 wol_usesecureonpassword;

//GLIB argument parser variables
GOptionEntry entries[] =
{
	{"mac", 'm', 0, G_OPTION_ARG_STRING, &macaddress, "MAC address", "M"},
	{"ip", 'i', 0, G_OPTION_ARG_STRING,  &ipaddress, "IP address", "I"},
	{"port", 'p', 0, G_OPTION_ARG_INT, &udpport, "UDP port", "P"},
	{"secureonpassword", 's', 0, G_OPTION_ARG_STRING, &secureonpassword, "SecureOn (tm) password", "S"},
#if defined(LANWAKEUP_UI_CONSOLE)
	{"console", 'c', 0, G_OPTION_ARG_NONE, &console, "Console", NULL},
#endif
#if defined(LANWAKEUP_UI_GTK)
	{"gtk+", 'g', 0, G_OPTION_ARG_NONE, &gtk, "GTK+", NULL},
#endif
	{NULL}
};

GOptionContext *context;

//===========================================================//
//Functions                                                  //
//===========================================================//

int main(int argc, char *argv[])
{
	int i = 0;

	//Initialize gettext support
	setlocale(LC_ALL,"");
	bindtextdomain("lanwakeup", "locales");
	textdomain("lanwakeup");

	//Parse arguments
	context = g_option_context_new("- Wake On LAN program for waking up your machines");

	g_option_context_add_main_entries(context, entries, NULL);

	if(g_option_context_parse(context, &argc, &argv, NULL) == FALSE)
	{
		printf("%s", g_option_context_get_help(context, TRUE, NULL));
		return 0;
	}

	 //Check number of interfaces
#if defined(LANWAKEUP_UI_CONSOLE)
        i += console;
#endif

#if defined(LANWAKEUP_UI_GTK)
        i += gtk;
#endif

	//Verify and put MAC address
	if(macaddress == NULL && i == 1)
	{

		wol_macaddress = WakeOnLAN_CreateMACAddressFromString("00:00:00:00:00:00");

	        if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
        	{
                	printf("Please, put a valid MAC address.\n");
	                return 0;
        	}

	}
	else
	{
		wol_macaddress = WakeOnLAN_CreateMACAddressFromString(macaddress);

		if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
		{
			printf("Please, put a valid MAC address.\n");
			return 0;
		}
	}

	//Verify and put UDP port
	if(udpport != 40000)
	{
		if(udpport < 0 || udpport > G_MAXUINT16)
		{
			printf("Please, put a valid UDP port.\n");
			return 0;
		}
	}

	//Verify and put SecureOn (tm) password
	if(secureonpassword == NULL)
	{

		wol_secureonpassword = WakeOnLAN_CreateSecureOnPasswordFromString("00:00:00:00:00:00");
		wol_usesecureonpassword = WAKEONLAN_FALSE;

	        if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
        	{
                	printf("Please, put a valid SecureOn (tm) password.\n");
	                return 0;
        	}

	}
	else
	{
		wol_secureonpassword = WakeOnLAN_CreateSecureOnPasswordFromString(secureonpassword);
		wol_usesecureonpassword = WAKEONLAN_TRUE;

		if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
		{
			printf("Please, put a valid SecureOn (tm) password.\n");
			return 0;
		}
	}

	wol_udpport = (WakeOnLAN_UInt16)udpport;

	//Verify and put IP address
	if(ipaddress == NULL)
	{
		wol_ipaddress = WakeOnLAN_CreateIPAddressFromString("255.255.255.255");

		if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
		{
			printf("Error creating the IP address.\n");
			return 0;
		}
	}
	else
	{
		wol_ipaddress = WakeOnLAN_CreateIPAddressFromString(ipaddress);

                if(WakeOnLAN_GetError() != WAKEONLAN_NO_ERROR)
                {
                        printf("Please, put a valid IP address.\n");
                        return 0;
                }
	}

	//Check selection of interfaces
	if(i == 0) //No interface selected
	{
		printf("Waking up machine...\n");

		if(WakeOnLAN_WakeupMachine(wol_macaddress, wol_ipaddress, wol_udpport, wol_secureonpassword, wol_usesecureonpassword) == WAKEONLAN_TRUE)
		{
			printf("Done.\n");
		}
		else
		{
			printf("Failed.\n");
		}

		return 0;
	}
	else if(i > 1) //More than 1 interface selected
        {
                printf("Please, select only one interface.\n");
		return 0;
        }

	//Preload selected interfaces
#if defined(LANWAKEUP_UI_CONSOLE)
	if(console == TRUE) //Console interface selected
	{
		interface.Load = LANWakeup_Console_LoadInterface;
	}
#endif
#if defined (LANWAKEUP_UI_GTK)
	else if(gtk == TRUE) //GTK+ interface selected
	{
		interface.Load = LANWakeup_GTK_LoadInterface;
	}
#endif

	//Load selected interface
	if(i == 1) // One interface selected
	{
		interface.Load(argc, argv, wol_macaddress, wol_ipaddress, wol_udpport, wol_secureonpassword, wol_usesecureonpassword);
	}

	return 0;
}
