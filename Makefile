################################
#Makefile for WakeOnLANLIB
################################

CC = gcc
SRC_DIR = src/
GUI_DIR = gui/
BIN_DIR = bin/
LOCALES_DIR = locales/
GLIB_CONFIG = `pkg-config --cflags --libs glib-2.0`
WAKEONLAN_CONFIG = `pkg-config --cflags --libs wakeonlan`
GTK_CONFIG = `pkg-config --cflags --libs gtk+-2.0 gmodule-2.0`
SYSTEM_FILES_DIR = /usr/share/
SYSTEM_BINARIES_DIR = /usr/bin/

COMPILE_CMD := $(CC) $(GLIB_CONFIG) $(WAKEONLAN_CONFIG)

ifeq "$(USE_CONSOLE_UI)" "y"
	COMPILE_CMD := $(COMPILE_CMD) -DLANWAKEUP_UI_CONSOLE
endif

ifeq "$(USE_GTK_UI)" "y"
	COMPILE_CMD := $(COMPILE_CMD) $(GTK_CONFIG) -DLANWAKEUP_UI_GTK
endif

COMPILE_CMD := $(COMPILE_CMD) -o $(BIN_DIR)lanwakeup $(SRC_DIR)*.c

all:
	#Compile library
	echo "Compiling LANWakeup..."
	$(COMPILE_CMD)
	echo "Copying GUI data..."
	cp $(GUI_DIR)LANWakeup.ui $(BIN_DIR)
	cp $(GUI_DIR)GUILogo.png $(BIN_DIR)
	cp $(GUI_DIR)Icon.png $(BIN_DIR)	
translations:
	#Create the locales
	echo "Making locales..."
	mkdir -p $(BIN_DIR)locales/es/LC_MESSAGES
	msgfmt -o $(BIN_DIR)locales/es/LC_MESSAGES/lanwakeup.mo $(LOCALES_DIR)es/LC_MESSAGES/lanwakeup.po
translations_clean:
	#Clean the locales
	echo "Cleaning locales..."
	rm -r $(BIN_DIR)locales
install:
	#Install headers
	echo "Making directory for LANWakeup if not exists..."
	mkdir -p $(SYSTEM_FILES_DIR)lanwakeup/
	cp -r $(BIN_DIR)* $(SYSTEM_FILES_DIR)lanwakeup/
	#Change permission to allow users execute LANWakeup
	chmod -R a+rx $(SYSTEM_FILES_DIR)lanwakeup/*	
	#Make the symbolic link
	echo "Installing LANWakeup..."
	echo -e "#!/bin/bash\ncd /usr/share/lanwakeup\n./lanwakeup \044\052" > $(SYSTEM_BINARIES_DIR)lanwakeup
	chmod a+rx $(SYSTEM_BINARIES_DIR)lanwakeup
#	ln -f $(SYSTEM_FILES_DIR)lanwakeup/lanwakeup $(SYSTEM_BINARIES_DIR)lanwakeup
uninstall:
	#Remove the components
	echo "Removing LANWakeup..."
	rm -r $(SYSTEM_FILES_DIR)lanwakeup
	rm $(SYSTEM_BINARIES_DIR)lanwakeup
clean:
	#Cleans compiling thrash
	echo "Cleaning thrash..."
	rm -rf $(BIN_DIR)*
